#include <stdlib.h>
#include <stdio.h>

int main() {
    int sum = 0;
    printf("The first 10 natural numbers are: ");
    for(int i=0; i<10; i++) {
        sum += i+1;
        printf("%i ", i+1);
    }
    printf("\n The sum is %i\n", sum);
}
