#include <stdlib.h>
#include <stdio.h>

double square(double n) {
    return n*n;
}

int main() {
    char buff[50];
    char* end;
    printf("Input any number for square: ");
    fgets(buff, 50, stdin);
    double n = strtod(buff, &end);
    double squared = square(n);
    printf("The square of %f is %f.\n", n, squared);
}
