#include <stdlib.h>
#include <stdio.h>

int main() {
    int sum = 0;
    printf("The first 10 natural numbers are: ");
    int i=0;
    while(i<10) {
        sum += i+1;
        printf("%i ", i+1);
        i++;
    }
    printf("\n The sum is %i\n", sum);
}
