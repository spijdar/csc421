#include <stdlib.h>
#include <stdio.h>

int even_or_odd(int n) {
    return n % 2 == 0 ? 0 : 1;
}

int main() {
    char buff[50];
    char* end;
    printf("Input any number: ");
    fgets(buff, 50, stdin);
    long n = strtol(buff, &end, 10);
    char* parity = even_or_odd(n) == 0 ? "even" : "odd";
    printf("The entered number is %s.\n", parity);
}
