#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node * next;
} node_t;

void print_list(node_t * head) {
    node_t * current = head;

    while (current != NULL) {
        printf("%d\n", current->val);
        current = current->next;
    }
}

int pop(node_t ** head) {
    int retval = -1;
    node_t * next_node = NULL;

    if (*head == NULL) {
        return -1;
    }

    next_node = (*head)->next;
    retval = (*head)->val;
    free(*head);
    *head = next_node;

    return retval;
}

void print_all(node_t ** head, int val) {
    node_t* node = *head;
    /* if the first node matches, print it */
    if(node->val == val) { 
        if(node->next != NULL) {
            printf("%x\n", node);
        }
    }
    /* as long as we have a 'next' node */
    for(; node->next != NULL; node = node->next) {
        if(node->next->val == val) { /* if the next node's value matches */
            /* print it */
            printf("%x\n", node->next);
        }
    }
}

int main() {

    node_t * test_list = malloc(sizeof(node_t));
    test_list->val = 1;
    test_list->next = malloc(sizeof(node_t));
    test_list->next->val = 2;
    test_list->next->next = malloc(sizeof(node_t));
    test_list->next->next->val = 3;
    test_list->next->next->next = malloc(sizeof(node_t));
    test_list->next->next->next->val = 4;
    test_list->next->next->next->next = NULL;

    print_all(&test_list, 2);
}
