#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int val;
    struct node * next;
} node_t;

void print_list(node_t * head) {
    node_t * current = head;

    while (current != NULL) {
        printf("%d\n", current->val);
        current = current->next;
    }
}

int pop(node_t ** head) {
    int retval = -1;
    node_t * next_node = NULL;

    if (*head == NULL) {
        return -1;
    }

    next_node = (*head)->next;
    retval = (*head)->val;
    free(*head);
    *head = next_node;

    return retval;
}

node_t* search_list(node_t ** head, int val) {
    node_t* node = *head;
    /* if the first node matches, return it */
    if(node->val == val) { 
        if(node->next != NULL) {
            return node;
        }
        else {
            return NULL;
        }
    }
    /* if the list is only one cons cell (and it doesn't match), abort */
    if(node->next == NULL) {
        return NULL;
    }
    /* as long as we have a 'next' node */
    for(; node->next != NULL; node = node->next) {
        if(node->next->val == val) { /* if the next node's value matches */
            /* return it */
            return node->next;
        }
    }
    return NULL;
}

int main() {

    node_t * test_list = malloc(sizeof(node_t));
    test_list->val = 1;
    test_list->next = malloc(sizeof(node_t));
    test_list->next->val = 2;
    test_list->next->next = malloc(sizeof(node_t));
    test_list->next->next->val = 3;
    test_list->next->next->next = malloc(sizeof(node_t));
    test_list->next->next->next->val = 4;
    test_list->next->next->next->next = NULL;

    node_t* node = search_list(&test_list, 3);

    printf("%d\n", node->val);
}
