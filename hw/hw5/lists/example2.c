#include <stdio.h>
#include <stdlib.h>

int main() {
    typedef struct node {
        int val;
        struct node * next;
    } node_t;

    node_t * head = NULL;
    head = malloc(sizeof(node_t));
    head->val = 1;
    head->next = malloc(sizeof(node_t));
    head->next->val = 2;
    head->next->next = NULL;

    return 0;
}

