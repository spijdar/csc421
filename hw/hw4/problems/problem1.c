#include <stdio.h>

int 
main()
{
    union {
        double f;
        unsigned long u;
    } p;

    p.f = -13.0;
    unsigned int sign = (p.u >> 63) & 1;
    unsigned int exp = (p.u >> 52) & 0x7ff;

    unsigned long f = 1;
    unsigned long coef_mask = (1ul << 52) - 1ul;
    unsigned long coef = p.u & coef_mask;

    printf("%lx\n", p.u);

    printf("%i\n", sign);
    printf("%i\n", exp);
    printf("0x%lx\n", coef);

    return 0;
}
