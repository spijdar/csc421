#include <stdio.h>

union exercise_union {
    int     ints[6];
    char    chars[21];
};

int
main()
{

    union exercise_union intCharacters = {{1853169737, 1936876900, 1684955508, 1768838432, 561213039, 0}};

    printf("[");

    int i;
    for (i = 0; i < 19; ++i)
        printf("%c, ", intCharacters.chars[i]);

    printf("%i]\n", intCharacters.chars[19]);

    printf("%s\n", intCharacters.chars);
    return 0;
}
