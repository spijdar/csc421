#include <stdio.h>

int 
main()
{
    union Coins change;
    for (int i = 0; i < sizeof(change) / sizeof(int); ++i) {
        scanf("%i", change.coins + i);
    }
    printf("There are %i quarters, %i dimes, %i nickels, and %i pennies\n",
           change.quarter, change.dime, change.nickel, change.penny);


    return 0;
}
