#include <stdlib.h>
#include <stdio.h>

double get_number() {
  printf("Enter a number: ");
  char input[10];
  fgets(input, 9, stdin);
  return atof(input);
}

void get_array(double* numbers) {
  numbers[0] = get_number();
  numbers[1] = get_number();
  numbers[2] = get_number();
}

int main() {
  double numbers[2][3];
  double merged[6];
  printf("Enter the first array's values:\n");
  get_array(numbers[0]);
  printf("Enter the second array's values:\n");
  get_array(numbers[1]);
  for(int i = 0, counter = 0; i < 2; i++) {
    for(int j = 0; j < 3; j++, counter++) {
      merged[counter] = numbers[i][j];
    }
  }
  printf("The merged array is [");
  for(int i = 0; i < 6; i++) {
    printf("%f, ", merged[i]);
  }
  printf("].\n");
}
