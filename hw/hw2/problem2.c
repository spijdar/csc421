#include <stdio.h>

int main() {
  double numbers[5] = { 5, 8, 10, 13, 19 };
  double sum;
  printf("The array is [");
  for(int i = 0; i < 5; i++) {
    printf("%f, ", numbers[i]);
    sum += numbers[i];
  }
  printf("].\n The sum of the array is %f.\n", sum);
}
