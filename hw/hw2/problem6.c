#include <stdlib.h>
#include <stdio.h>

void swap(int* a, int* b) {
  *a ^= *b;
  *b ^= *a;
  *a ^= *b;
}

void sort(int* numbers, int length) {
  if (length < 2) return;
  int pivot = numbers[length / 2];
  int i, j;
  for(i = 0, j = length - 1; ; i++, j--) {
    while(numbers[i] < pivot) i++;
    while(numbers[j] > pivot) j--;

    if(i >= j) break;

    swap(numbers + i, numbers + j);
  }
  sort(numbers, i);
  sort(numbers + i, length - i);
}

int main() {
  printf("How many values to sort (<100)? ");
  char input[10];
  fgets(input, 4, stdin);
  int count = atoi(input);
  int* numbers = malloc(count);
  for (int i = 0; i < count; i++) {
    printf("Enter a number: ");
    fgets(input, 4, stdin);
    int num = atoi(input);
    numbers[i] = num;
  }
  sort(numbers, count);
  for(int i = 0; i < count; i++) {
    printf("%i\n", numbers[i]);
  }
}
