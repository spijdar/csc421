#include<stdio.h>

int main() {
	int a = 125, b = 12345; 
	long ax = 1234567890; 
	short s = 4043; 
	float x = 2.13459; 
	double dx = 1.1415927; 
	char c = '4'; 
	unsigned long ux = 2541567890;

	printf("a + c = %i\n", a + c);
	printf("x + c = %i\n", x + c);
	printf("dx + x = %g\n", dx + x);
	printf("((int) dx) + ax = %i\n", ((int) dx) + ax);
	printf("a + x = %f\n", a + x);
	printf("s + b = %i\n", s + b);
	printf("ax + b = %u\n", ax + b);
	printf("s + c = %i\n", s + c);
	printf("ax + c = %i\n", ax + c);
	printf("ax + ux = %u\n", ax + ux);
}
